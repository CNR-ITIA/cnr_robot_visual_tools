#include <phri_teleop_gui.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(phri_teleop_gui::PHRITeleop,rviz::Panel)


namespace phri_teleop_gui
{

  PHRITeleop::PHRITeleop(QWidget* parent) : Panel(parent)
  {
  }

  void PHRITeleop::onInitialize()
  {


    m_tab1 = new QTabWidget();
    m_w1 = new QWidget();

    //Main Layout
    m_bp = new basic_panel_gui::BasicPanel();

    //Force Teleop
    m_ft = new force_teleop_gui::ForceTeleop(m_bp);
    m_w1->setLayout(m_ft->m_grid_layout1);

    m_tab2 = new QTabWidget();
    m_tab2->addTab(m_w1, "Force Teleop");

    m_grid_layout3 = new QVBoxLayout();
    m_grid_layout3->addWidget(m_tab2);


    m_grid_layout2 = new QVBoxLayout();
    m_grid_layout2->addLayout(m_grid_layout3);
    m_grid_layout2->addLayout(m_bp->m_layout7);

    m_w2 = new QWidget();
    m_w2->setLayout(m_grid_layout2);
    m_tab1->addTab(m_w2,QString::fromStdString("pHRI Interface Teleop") );


    m_grid_layout1 = new QVBoxLayout(this);
    m_grid_layout1->addWidget(m_tab1);

  }


  PHRITeleop::~PHRITeleop()
  {
    // delete m_grid_layout1;
    // delete m_w2;
    // delete m_grid_layout3;
    // delete m_tab2;
    // delete m_ft;
    // delete m_bp;
    // delete m_w2;
    // delete m_w1;
    // delete m_tab1;
    // delete m_tab2, m_tab1;
    // delete m_w2;
    // delete m_w1;

    // ROS_INFO("Deleted all allocated object of class PHRITeleop()");
  }



}









