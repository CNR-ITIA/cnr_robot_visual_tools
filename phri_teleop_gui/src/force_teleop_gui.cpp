#include <force_teleop_gui.h>


namespace force_teleop_gui
{

// ChartForce::~ChartForce(){};

ChartForce::ChartForce()
{
  //CHART
  m_chart = new QtCharts::QChart();

  QPen pen;
  pen.setWidth(2);
  
  m_seriesX = new QtCharts::QLineSeries();
  pen.setColor(QColor(255,63,63));
  m_seriesX->setPen(pen);
  m_seriesY = new QtCharts::QLineSeries();
  pen.setColor(QColor(44,195,135));
  m_seriesY->setPen(pen);
  m_seriesZ = new QtCharts::QLineSeries();
  pen.setColor(QColor(51,102,203));
  m_seriesZ->setPen(pen);
  
  y_lim_chart = new QtCharts::QValueAxis();
  y_lim_chart->setRange(-100,100);
  y_lim_chart->setTitleText("Force [N]");

  m_chart->addAxis(y_lim_chart, Qt::AlignLeft);

  x_lim_chart = new QtCharts::QValueAxis();
  x_lim_chart->setRange(-100,0);
  // x_lim_chart->setTitleText("seq");

  m_chart->addAxis(x_lim_chart, Qt::AlignBottom);

  m_chart->legend()->hide();

  m_chart->addSeries(m_seriesX);
  m_chart->addSeries(m_seriesY);
  m_chart->addSeries(m_seriesZ);

  m_seriesX->attachAxis(y_lim_chart);
  m_seriesY->attachAxis(y_lim_chart);
  m_seriesZ->attachAxis(y_lim_chart);

  m_seriesX->attachAxis(x_lim_chart);
  m_seriesY->attachAxis(x_lim_chart);
  m_seriesZ->attachAxis(x_lim_chart);

  chart_view = new QtCharts::QChartView(m_chart);
  chart_view->repaint();

  m_pts_idx = 0;
  m_pts_idx2 = 0;


}


void ChartForce::updateChart(double F_x, double F_y, double F_z)
{
  int max_x_axis_points = 250;
  m_seriesX->append(m_pts_idx, F_x);
  m_seriesY->append(m_pts_idx, F_y);
  m_seriesZ->append(m_pts_idx, F_z);

  m_chart->removeAxis(x_lim_chart);
  x_lim_chart = new QtCharts::QValueAxis();
  x_lim_chart->setRange(m_pts_idx2-max_x_axis_points,m_pts_idx2++);
  // x_lim_chart->setTitleText("seq");
  m_chart->addAxis(x_lim_chart, Qt::AlignBottom);

  if (m_pts_idx > max_x_axis_points)
  {
    m_seriesX->remove(1);
    m_seriesY->remove(1);
    m_seriesZ->remove(1);
  }

  m_seriesX->attachAxis(x_lim_chart);
  m_seriesY->attachAxis(x_lim_chart);
  m_seriesZ->attachAxis(x_lim_chart);

  m_pts_idx++;
  chart_view->repaint();


  return;
}


// DriveWidget::~DriveWidget(){};

DriveWidget::DriveWidget(basic_panel_gui::BasicPanel* basic_panel, QComboBox* dropdown_menu, std::vector<QPushButton*> button, std::shared_ptr<ChartForce>& chart_force)
{

  m_pub_ts=m_nh.advertise<geometry_msgs::WrenchStamped>("/ft_wrench",1);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_dropdown_menu = new QComboBox();
  m_dropdown_menu = dropdown_menu;

  m_frame_button = button;

  m_chart_force = chart_force;

  m_timer = new QTimer();
  connect(m_timer, SIGNAL(timeout()), this, SLOT(publishTwist()));

}


void DriveWidget::publishTwist()
{
  if (m_frame_button.at(0)->isChecked())
    m_msg_ws.header.frame_id = "BASE";
  else if (m_frame_button.at(1)->isChecked())
    m_msg_ws.header.frame_id = "TOOL";

  if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XY"))
  {
    m_msg_ws.wrench.force.x = m_slider->value()*m_mouse_x;
    m_msg_ws.wrench.force.y = m_slider->value()*m_mouse_y;
  }
  else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XZ"))
  {
    m_msg_ws.wrench.force.x = m_slider->value()*m_mouse_x;
    m_msg_ws.wrench.force.z = m_slider->value()*m_mouse_y;
  }
  else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane YZ"))
  {
    m_msg_ws.wrench.force.y = m_slider->value()*m_mouse_x;
    m_msg_ws.wrench.force.z = m_slider->value()*m_mouse_y;
  }

  m_pub_ts.publish(m_msg_ws);

  m_chart_force->updateChart(m_msg_ws.wrench.force.x,m_msg_ws.wrench.force.y,m_msg_ws.wrench.force.z);

}


void DriveWidget::paintEvent(QPaintEvent *event)
{
    QColor background = Qt::gray;
    int w = width();
    int h = height();
    m_size = (( w > h ) ? h : w) - 1;
    m_hpad = ( w - m_size ) / 2;
    m_vpad = ( h - m_size ) / 2;

    m_painter = new QPainter(this);
    m_painter->setBrush( background );
    m_painter->setPen(Qt::black);
    m_painter->drawRect( QRect( m_hpad, m_vpad, m_size, m_size));

    if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XY"))
    {
        m_painter->setPen(Qt::red);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::green);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XZ"))
    {
        m_painter->setPen(Qt::red);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::blue);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane YZ"))
    {
        m_painter->setPen(Qt::green);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::blue);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }

    m_painter->end();
}


void DriveWidget::mouseMoveEvent( QMouseEvent* event )
{
    m_mouse_x = m_hpad-event->x();
    m_mouse_x = -(m_hpad-event->x() + m_size/2);
    m_mouse_x =  m_mouse_x / (m_size/2);

    if (m_mouse_x > 1 || m_mouse_x < -1)
      m_mouse_x = 0.0;

    m_mouse_y = m_vpad-event->y();
    m_mouse_y = (m_vpad-event->y() + m_size/2);
    m_mouse_y =  m_mouse_y / (m_size/2);

    if (m_mouse_y > 1 || m_mouse_y < -1)
      m_mouse_y = 0.0;

}

void DriveWidget::mousePressEvent( QMouseEvent* event )
{

    m_mouse_x = m_hpad-event->x();
    m_mouse_x = -(m_hpad-event->x() + m_size/2);
    m_mouse_x =  m_mouse_x / (m_size/2);

    if (m_mouse_x > 1 || m_mouse_x < -1)
      m_mouse_x = 0.0;

    m_mouse_y = m_vpad-event->y();
    m_mouse_y = (m_vpad-event->y() + m_size/2);
    m_mouse_y =  m_mouse_y / (m_size/2);

    if (m_mouse_y > 1 || m_mouse_y < -1)
      m_mouse_y = 0.0;

    m_timer->start(5); //time specified in ms
}

void DriveWidget::mouseReleaseEvent( QMouseEvent* event )
{
    m_timer->stop();
    m_mouse_x = 0.0;
    m_mouse_y = 0.0;

    m_msg_ws.wrench.force.x = 0.0;
    m_msg_ws.wrench.force.y = 0.0;
    m_msg_ws.wrench.force.z = 0.0;
    m_pub_ts.publish(m_msg_ws);
}



ForceTeleop::~ForceTeleop(){
  ROS_ERROR("qua");
}

ForceTeleop::ForceTeleop(basic_panel_gui::BasicPanel* basic_panel)
{
  m_pub_ts=m_nh.advertise<geometry_msgs::WrenchStamped>("/ft_wrench",1);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_txt_edit = new QTextEdit();
  m_txt_edit = basic_panel->m_txt_edit;

  m_button1.resize(2);
  m_button2.resize(2);
  m_button3.resize(2);
  m_button4.resize(2);
  m_button5.resize(2);
  m_button6.resize(2);
  m_button7.resize(2);

  //FRONT, BACK, LEFT, RIGHT
  QString style_sheet="QGroupBox{border:1px solid gray;border-radius:5px;margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin;subcontrol-position:top center;padding:0 3px;}";
  std::string path_to_images = ros::package::getPath("phri_teleop_gui");

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/up_arrow_y2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button1.at(0) = new QPushButton;
  m_button1.at(0)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button1.at(0)->setIcon(*m_button_icon);
  m_button1.at(0)->setIconSize(m_pixmap->rect().size());
  m_button1.at(0)->setAutoRepeat(true);
  m_button1.at(0)->setAutoRepeatInterval(5);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/down_arrow_y2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button1.at(1) = new QPushButton;
  m_button1.at(1)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button1.at(1)->setIcon(*m_button_icon);
  m_button1.at(1)->setIconSize(m_pixmap->rect().size());
  m_button1.at(1)->setAutoRepeat(true);
  m_button1.at(1)->setAutoRepeatInterval(5);

  m_grid_layout3 = new QGridLayout();
  m_grid_layout3->addWidget(m_button1.at(0),0,1,1,1);
  m_grid_layout3->addWidget(m_button1.at(1),2,1,1,1);


  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/right_arrow_x2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button2.at(0) = new QPushButton;
  m_button2.at(0)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button2.at(0)->setIcon(*m_button_icon);
  m_button2.at(0)->setIconSize(m_pixmap->rect().size());
  m_button2.at(0)->setAutoRepeat(true);
  m_button2.at(0)->setAutoRepeatInterval(5);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/left_arrow_x2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button2.at(1) = new QPushButton;
  m_button2.at(1)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button2.at(1)->setIcon(*m_button_icon);
  m_button2.at(1)->setIconSize(m_pixmap->rect().size());
  m_button2.at(1)->setAutoRepeat(true);
  m_button2.at(1)->setAutoRepeatInterval(5);

  m_grid_layout3->addWidget(m_button2.at(0),1,2,1,1);
  m_grid_layout3->addWidget(m_button2.at(1),1,0,1,1);


  m_button7.at(0) = new QPushButton;
  m_button7.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Maximum);
  m_button7.at(0)->setAutoRepeat(true);
  m_button7.at(0)->setAutoRepeatInterval(5);
  m_button7.at(0)->setText("Base frame");
  m_button7.at(0)->setCheckable(true);
  m_button7.at(0)->setChecked(false);

  m_button7.at(1) = new QPushButton;
  m_button7.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Maximum);
  m_button7.at(1)->setAutoRepeat(true);
  m_button7.at(1)->setAutoRepeatInterval(5);
  m_button7.at(1)->setText("Tool frame");
  m_button7.at(1)->setCheckable(true);
  m_button7.at(1)->setChecked(true);
  m_msg_ws.header.frame_id="TOOL"; //If no frame is initially checked, default is TOOL

  m_grid_layout3->addWidget(m_button7.at(0),3,0,1,3);
  m_grid_layout3->addWidget(m_button7.at(1),4,0,1,3);

  m_group1 = new QGroupBox();
  m_group1->setTitle("XYZ");
  m_group1->setStyleSheet(style_sheet);
  m_group1->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
  m_group1->setLayout(m_grid_layout3);
  m_group1->hasHeightForWidth();

  m_grid_layout1 = new QGridLayout();
  m_grid_layout1->addWidget(m_group1,0,0,1,1);



  //UP AND DOWN
  m_grid_layout4 = new QGridLayout();

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/up_arrow_z2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button5.at(0) = new QPushButton;
  m_button5.at(0)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button5.at(0)->setIcon(*m_button_icon);
  m_button5.at(0)->setIconSize(m_pixmap->rect().size());
  m_button5.at(0)->setAutoRepeat(true);
  m_button5.at(0)->setAutoRepeatInterval(5);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/down_arrow_z2.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button5.at(1) = new QPushButton;
  m_button5.at(1)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_button5.at(1)->setIcon(*m_button_icon);
  m_button5.at(1)->setIconSize(m_pixmap->rect().size());
  m_button5.at(1)->setAutoRepeat(true);
  m_button5.at(1)->setAutoRepeatInterval(5);

  m_grid_layout3->addWidget(m_button5.at(0),0,2,1,1);
  m_grid_layout3->addWidget(m_button5.at(1),2,2,1,1);



  //CHART
  m_chart_force.reset(new ChartForce());
  m_chart_force->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  m_chart_force->setMinimumSize(500,500);

  m_layout7 = new QVBoxLayout();
  m_layout7->addWidget(m_chart_force->chart_view);

  m_group6 = new QGroupBox();
  m_group6->setTitle("Chart");
  m_group6->setStyleSheet(style_sheet);
  m_group6->setLayout(m_layout7);
  m_group6->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  m_group6->setMinimumSize(150,200);

  m_grid_layout1->addWidget(m_group6,1,0,-1,-1);

  //FREE MOVEMENT
  m_dropdown_menu = new QComboBox();
  m_dropdown_menu->addItem("Plane XY");
  m_dropdown_menu->addItem("Plane XZ");
  m_dropdown_menu->addItem("Plane YZ");


  m_layout5 = new QVBoxLayout();
  m_layout5->addWidget(m_dropdown_menu);

  m_drive_widget = new DriveWidget(basic_panel, m_dropdown_menu, m_button7, m_chart_force);
  m_layout5->addWidget(m_drive_widget);
  m_drive_widget->hasHeightForWidth();
  m_layout5->hasHeightForWidth();

  m_group4 = new QGroupBox();
  m_group4->setTitle("Free Move");
  m_group4->setStyleSheet(style_sheet);
  m_group4->setLayout(m_layout5);
  m_group4->setMinimumSize(200,200);
  m_group4->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


  m_grid_layout1->addWidget(m_group4,0,2,1,-1);


  connect(m_button1.at(0), SIGNAL(pressed()), this, SLOT(moveY()));
  connect(m_button1.at(0), SIGNAL(released()), this, SLOT(resetY()));
  connect(m_button1.at(1), SIGNAL(pressed()), this, SLOT(moveY()));
  connect(m_button1.at(1), SIGNAL(released()), this, SLOT(resetY()));
  connect(m_button2.at(0), SIGNAL(pressed()), this, SLOT(moveX()));
  connect(m_button2.at(0), SIGNAL(released()), this, SLOT(resetX()));
  connect(m_button2.at(1), SIGNAL(pressed()), this, SLOT(moveX()));
  connect(m_button2.at(1), SIGNAL(released()), this, SLOT(resetX()));
  connect(m_button7.at(0), SIGNAL(clicked()), this, SLOT(selectBaseFrame()));
  connect(m_button7.at(1), SIGNAL(clicked()), this, SLOT(selectToolFrame()));
  connect(m_button5.at(0), SIGNAL(pressed()), this, SLOT(moveZ()));
  connect(m_button5.at(0), SIGNAL(released()), this, SLOT(resetZ()));
  connect(m_button5.at(1), SIGNAL(pressed()), this, SLOT(moveZ()));
  connect(m_button5.at(1), SIGNAL(released()), this, SLOT(resetZ()));


  connect(m_dropdown_menu, SIGNAL(activated(int)), m_drive_widget, SLOT(update()));


}


//force Teleop Methods
void ForceTeleop::moveX()
{
  setTwistZero(m_button2.at(0));
  if (m_button2.at(0)->isDown())
    m_msg_ws.wrench.force.x = m_slider->value();
  else
    m_msg_ws.wrench.force.x = -m_slider->value();

  m_msg_ws.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ws);

  m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);
}

void ForceTeleop::resetX()
{
  if (!m_button2.at(0)->isDown() && !m_button2.at(1)->isDown())
  {
    m_msg_ws.wrench.force.x = 0.0;
    m_msg_ws.wrench.force.y = 0.0;
    m_msg_ws.wrench.force.z = 0.0;

    m_msg_ws.header.stamp=ros::Time::now();
    m_pub_ts.publish(m_msg_ws);

    m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);
  }
}

void ForceTeleop::moveY()
{
  if (m_button1.at(0)->isDown())
    m_msg_ws.wrench.force.y = m_slider->value();
  else
    m_msg_ws.wrench.force.y = -m_slider->value();

  m_msg_ws.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ws);

  m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);

}

void ForceTeleop::resetY()
{
  if (!m_button1.at(0)->isDown() && !m_button1.at(1)->isDown())
  {
    m_msg_ws.wrench.force.x = 0.0;
    m_msg_ws.wrench.force.y = 0.0;
    m_msg_ws.wrench.force.z = 0.0;

    m_msg_ws.header.stamp=ros::Time::now();
    m_pub_ts.publish(m_msg_ws);

    m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);
  }
}

void ForceTeleop::moveZ()
{
  if (m_button5.at(0)->isDown())
    m_msg_ws.wrench.force.z = m_slider->value();
  else
    m_msg_ws.wrench.force.z = -m_slider->value();

  m_msg_ws.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ws);

  m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);
}

void ForceTeleop::resetZ()
{
  if (!m_button5.at(0)->isDown() && !m_button5.at(1)->isDown())
  {
    m_msg_ws.wrench.force.x = 0.0;
    m_msg_ws.wrench.force.y = 0.0;
    m_msg_ws.wrench.force.z = 0.0;

    m_msg_ws.header.stamp=ros::Time::now();
    m_pub_ts.publish(m_msg_ws);

    m_chart_force->updateChart(m_msg_ws.wrench.force.x, m_msg_ws.wrench.force.y, m_msg_ws.wrench.force.z);
  }
}

void ForceTeleop::rotateX()
{
}

void ForceTeleop::rotateY()
{
}

void ForceTeleop::rotateZ()
{
}

void ForceTeleop::resetTwist(QPushButton* button)
{
}

void ForceTeleop::setTwistZero(QPushButton* button)
{
}

void ForceTeleop::selectBaseFrame()
{
  m_msg_ws.header.frame_id="BASE";
  m_button7.at(1)->setChecked(false);
  m_button7.at(0)->setChecked(true);
}

void ForceTeleop::selectToolFrame()
{
  m_msg_ws.header.frame_id="TOOL";
  m_button7.at(0)->setChecked(false);
  m_button7.at(1)->setChecked(true);
}







}





