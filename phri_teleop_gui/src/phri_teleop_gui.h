#ifndef robots_teleop_gui_201812101007
#define robots_teleop_gui_201812101007

#include <ros/ros.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include "rviz/panel.h"

#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <QSpacerItem>
#include <QTabWidget>
#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QComboBox>
#include <QTextEdit>

#include "basic_panel_gui.h"
#include "force_teleop_gui.h"
#include <QtWidgets/QAction>


namespace phri_teleop_gui
{
  class PHRITeleop: public rviz::Panel
  {
    
  Q_OBJECT

  public Q_SLOTS:

  public:
    PHRITeleop( QWidget* parent = 0 );
    virtual ~PHRITeleop();
    virtual void onInitialize();
    /** @brief Load the given html file. */
    

  protected:
    basic_panel_gui::BasicPanel* m_bp;
    force_teleop_gui::ForceTeleop* m_ft;

    QVBoxLayout* m_grid_layout1;
    QVBoxLayout* m_grid_layout2;
    QVBoxLayout* m_grid_layout3;
    QTabWidget* m_tab1;
    QTabWidget* m_tab2;
    QWidget* m_w1;
    QWidget* m_w2;

  };
}
  
#endif
