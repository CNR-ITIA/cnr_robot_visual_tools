#ifndef force_teleop_gui_201812101007
#define force_teleop_gui_201812101007

#include <ros/ros.h>
#include <ros/package.h>
#include <geometry_msgs/WrenchStamped.h>
#include <QPushButton>
#include <QGridLayout>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <QSpacerItem>
#include <QObject>
#include <QSlider>
#include <QTextEdit>
#include <QComboBox>
#include <QPainter>
#include <QRect>
#include <QWidget>
#include <QtGui>
#include <QTimer>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QAction>
#include <QObject>

#include <basic_panel_gui.h>


namespace force_teleop_gui
{

class ChartForce : public QWidget
{
    Q_OBJECT

public Q_SLOTS:
  

public:
  ChartForce();
  void updateChart(double F_x, double F_y, double F_z);
  QtCharts::QChartView *chart_view;


protected:
  QtCharts::QChart* m_chart;
  int m_pts_idx;
  int m_pts_idx2;
  QtCharts::QLineSeries* m_seriesX;
  QtCharts::QLineSeries* m_seriesY;
  QtCharts::QLineSeries* m_seriesZ;

  QtCharts::QValueAxis* x_lim_chart;
  QtCharts::QValueAxis* y_lim_chart;

};


class DriveWidget : public QWidget
{
    Q_OBJECT

public Q_SLOTS:
  void publishTwist();

public:
   DriveWidget(basic_panel_gui::BasicPanel* basic_panel, QComboBox* dropdown_menu, std::vector<QPushButton*> button, std::shared_ptr<ChartForce>& chart_force);


protected:
  void paintEvent(QPaintEvent *event);
  virtual void mouseMoveEvent( QMouseEvent* event );
  virtual void mousePressEvent( QMouseEvent* event );
  virtual void mouseReleaseEvent( QMouseEvent* event );

  ros::NodeHandle m_nh;
  ros::Publisher m_pub_ts;
  ros::Subscriber m_sub_tmp;
  geometry_msgs::WrenchStamped m_msg_ws;

  int m_hpad;
  int m_vpad;
  int m_size;
  double m_mouse_x;
  double m_mouse_y;
  
  QPainter* m_painter;
  QPushButton* m_fake_button;
  QTimer* m_timer;
  QSlider* m_slider;
  QComboBox* m_dropdown_menu;
  std::vector<QPushButton*> m_frame_button;

  std::shared_ptr<ChartForce> m_chart_force;

};


  class ForceTeleop : public QObject
  {

  Q_OBJECT

  public Q_SLOTS:
    void moveX();
    void moveY();
    void moveZ();
    void resetX();
    void resetY();
    void resetZ();
    void rotateX();
    void rotateY();
    void rotateZ();
    void setTwistZero(QPushButton* button);
    void resetTwist(QPushButton* button);
    void selectBaseFrame();
    void selectToolFrame();
    void updateChart();

  public:
    ForceTeleop(basic_panel_gui::BasicPanel* basic_panel);
    virtual ~ForceTeleop();

    ros::NodeHandle m_nh;
    ros::Publisher m_pub_ts;
    geometry_msgs::WrenchStamped m_msg_ws;
    
    std::vector<QPushButton*> m_button1;
    std::vector<QPushButton*> m_button2;
    std::vector<QPushButton*> m_button3;
    std::vector<QPushButton*> m_button4;
    std::vector<QPushButton*> m_button5;
    std::vector<QPushButton*> m_button6;    
    std::vector<QPushButton*> m_button7;
    QPushButton* m_button99;
    QGridLayout* m_grid_layout1;
    QGridLayout* m_grid_layout2;
    QGridLayout* m_grid_layout3;
    QGridLayout* m_grid_layout4;
    QVBoxLayout* m_layout5;
    QVBoxLayout* m_layout6;
    QVBoxLayout* m_layout7;
    QGroupBox* m_group1;
    QGroupBox* m_group2;
    QGroupBox* m_group3;
    QGroupBox* m_group4;
    QGroupBox* m_group5;
    QGroupBox* m_group6;
    QSlider* m_slider;
    QTextEdit* m_txt_edit;
    QTextEdit* m_txt_edit2;

    QComboBox* m_dropdown_menu;
    DriveWidget* m_drive_widget;

    QPixmap* m_pixmap;
    QIcon* m_button_icon;
    QSpacerItem* m_spacer;

    QtCharts::QChart* m_chart;
    QtCharts::QChartView *m_chart_view;
    int m_pts_idx;
    int m_pts_idx2;
    QtCharts::QLineSeries* m_seriesX;
    QtCharts::QLineSeries* m_seriesY;
    QtCharts::QLineSeries* m_seriesZ;

    QtCharts::QValueAxis* x_lim_chart;
    QtCharts::QValueAxis* y_lim_chart;

    std::shared_ptr<ChartForce> m_chart_force;

  };



}
  
#endif
