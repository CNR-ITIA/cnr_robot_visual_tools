#include <te_teleop_gui.h>


namespace teach_teleop_gui 
{


TeTeleop::~TeTeleop()
{
}


TeTeleop::TeTeleop(basic_panel_gui::BasicPanel* basic_panel, std::string hi_namespace, std::string ctrl_namespace, std::vector<std::string>* controlled_joints, std::vector<joint_limits_interface::JointLimits> joints_limits)
{
  m_hi_namespace = hi_namespace;
  m_ctrl_namespace = ctrl_namespace;

  m_pub_js=m_nh.advertise<sensor_msgs::JointState>("/"+m_ctrl_namespace+"/target_joint_teleop",1);

  m_vmax = new std::vector<double>;
  for (int i=0; i < joints_limits.size(); i++)
    m_vmax->push_back(joints_limits.at(i).max_velocity);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_txt_edit = new QTextEdit();
  m_txt_edit = basic_panel->m_txt_edit;

  m_controlled_joints = new std::vector<std::string>;
  m_controlled_joints = controlled_joints;

  m_nAx = m_controlled_joints->size();

  std::string feedback_joint_state_topic_name = "/"+m_hi_namespace+"/feedback_joint_state_topic";
  if( !m_nh.getParam("/"+m_hi_namespace+"/feedback_joint_state_topic", feedback_joint_state_topic_name))
  {
    m_txt_edit->append(QString::fromStdString("No parameter /"+m_hi_namespace+"/feedback_joint_state_topic defined in the configuration file."));
    ROS_ERROR("No parameter /%s/feedback_joint_state_topic defined in the configuration file.", m_hi_namespace.c_str());
  }


  m_joint_target_rec.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_nh,feedback_joint_state_topic_name, 1,boost::bind(&TeTeleop::callback,this,_1)));


  m_path_to_config = ros::package::getPath("robots_teleop_gui")+"/config";
  QString style_sheet="QGroupBox{border:1px solid gray;border-radius:5px;margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin;subcontrol-position:top center;padding:0 3px;}";

  std::string robot_folder = "mkdir -p "+m_path_to_config+"/positions"+"/"+m_hi_namespace;
  if (system( robot_folder.c_str() )!=0)
    ROS_WARN("Unable to create folder: %s to store positions. Maybe it alredy exists", robot_folder.c_str());


  //FIRST GROUP
  m_table = new QTableWidget;
  m_table->insertColumn(0);
  m_table->setHorizontalHeaderItem(0,new QTableWidgetItem("Name"));
  m_table->insertColumn(1);
  m_table->setHorizontalHeaderItem(1,new QTableWidgetItem("Date"));
  m_table->verticalHeader()->setVisible(false);
  m_table->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff);
  m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
  m_table->setSelectionMode(QAbstractItemView::SingleSelection);
  m_table->setMinimumWidth(m_table->horizontalHeader()->length()+15);
  m_table->setMinimumHeight(250);
  m_table->horizontalHeader()->setStretchLastSection(true);
  
  m_grid_layout1 = new QGridLayout();
  m_grid_layout1->addWidget(m_table,0,0,-1,-1);
  
  m_group1 = new QGroupBox();
  m_group1->setLayout(m_grid_layout1);
  m_group1->setStyleSheet(style_sheet);
  m_group1->setTitle(QString::fromStdString("Saved positions"));


  //SECOND GROUP
  m_txt_edit1 = new QTextEdit();
  m_txt_edit1->setText(QString::fromStdString("Insert new postion name here"));
//   m_txt_edit1->setMaximumHeight(10); 
  
  m_txt_edit1->sizeHint().setHeight(10);
  m_txt_edit1->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Preferred);
  
  m_button1 = new QPushButton();
  m_button1->setText(QString::fromStdString("Save"));
  
  m_button3 = new QPushButton();
  m_button3->setText(QString::fromStdString("Load"));
  
  m_button2 = new QPushButton();
  m_button2->setText(QString::fromStdString("Delete"));
  
  m_button4 = new QPushButton();
  m_button4->setText(QString::fromStdString("Move"));
  m_button4->setAutoRepeat(true);
  
  m_grid_layout3 = new QVBoxLayout();
  m_grid_layout3->addWidget(m_button4);
  m_grid_layout3->addWidget(m_txt_edit1);  
  m_grid_layout3->addWidget(m_button1);
  m_grid_layout3->addWidget(m_button3);
  m_grid_layout3->addWidget(m_button2);

  m_group2 = new QGroupBox();
  m_group2->setLayout(m_grid_layout3);
  m_group2->setStyleSheet(style_sheet);
  m_group2->setTitle(QString::fromStdString("Manage positions"));


  //PUT GROUPS IN MAIN LAYOUT
  m_grid_layout4 = new QGridLayout();
  m_grid_layout4->addWidget(m_group1,0,0,-1,1);
  m_grid_layout4->addWidget(m_group2,0,1,-1,-1);

  connect(m_button4, SIGNAL(pressed()), this, SLOT(moveToPosition()));
  connect(m_button4, SIGNAL(released()), this, SLOT(moveToPosition()));
  connect(m_button1, SIGNAL(clicked()), this, SLOT(savePosition()));
  connect(m_button2, SIGNAL(clicked()), this, SLOT(deletePosition()));
  connect(m_button3, SIGNAL(clicked()), this, SLOT(loadPositions()));

  ////Load current save positions for TeachTeleop
  loadPositions();

}



void TeTeleop::callback(const sensor_msgs::JointStateConstPtr msg)
{
}


void TeTeleop::savePosition()
{

  if (!m_joint_target_rec->isANewDataAvailable())
  {
    m_txt_edit->append(QString::fromStdString("No new messages available! An active controller of type itia/control/JointTeleopController is required to save new positions."));
    ROS_ERROR("No new messages available! An active controller of type itia/control/JointTeleopController is required to save new positions");
    return;
  }

  bool check = false;
  for (int i=0; i<m_table->rowCount(); i++)
  {
    if (!std::strcmp(m_table->item(i,0)->text().toStdString().c_str(), m_txt_edit1->toPlainText().toStdString().c_str() ))
      check = true;
  }
  if (!check)
  {
    registerPosition();

    //Store teach position info into a parameter
    std::string teach_parameter_name = "/te_info_position/controlled_joints ";
    std::string string_to_save_yaml =  "rosparam set "+teach_parameter_name+"\"[" ;
    for (int i=0; i<m_controlled_joints->size()-1; i++)
      string_to_save_yaml.append(m_controlled_joints->at(i)+",");
    string_to_save_yaml.append(m_controlled_joints->back()+"]\" ");
    if (system( string_to_save_yaml.c_str() )!=0)
    {
      m_txt_edit->append(QString::fromStdString("Unable to store parameter: "+teach_parameter_name));
      ROS_ERROR("Unable to store yaml parameters");
      return;
    }

    teach_parameter_name = "/te_info_position/target_pos ";
    string_to_save_yaml =  "rosparam set "+teach_parameter_name+"\"[" ;
    for (int i=0; i<m_controlled_joints->size()-1; i++)
      string_to_save_yaml.append(std::to_string(m_joint_target_rec->getData().position.at(i))+",");
    string_to_save_yaml.append(std::to_string(m_joint_target_rec->getData().position.back())+"]\" ");
    if (system( string_to_save_yaml.c_str() )!=0)
    {
      m_txt_edit->append(QString::fromStdString("Unable to store parameter: "+teach_parameter_name));
      ROS_ERROR("Unable to store yaml parameters");
      return;
    }

    //Store Position Time
    std::time_t now = std::time(0);
    std::tm *ltm = localtime(&now);
    std::string date = std::to_string(ltm->tm_mday)+"/"+std::to_string(ltm->tm_mon+1)+","+std::to_string(ltm->tm_hour)+":"+std::to_string(ltm->tm_min);
    teach_parameter_name = "/te_info_position/time_data ";
    string_to_save_yaml =  "rosparam set "+teach_parameter_name+date;
    if (system( string_to_save_yaml.c_str() )!=0)
    {
      m_txt_edit->append(QString::fromStdString("Unable to store parameter: "+teach_parameter_name));
      ROS_ERROR("Unable to store yaml parameters");
      return;
    }

    //Save on a .YAML file
    string_to_save_yaml =   "rosparam dump "+m_path_to_config+"/positions"+"/"+m_hi_namespace+"/"+m_txt_edit1->toPlainText().toStdString()+".yaml /te_info_position";
    if (system( string_to_save_yaml.c_str() )!=0)
    {
      m_txt_edit->append(QString::fromStdString("Unable to save .YAML file: "+m_txt_edit1->toPlainText().toStdString()+".yaml"));
      ROS_ERROR("Unable to save .YAML file: %s.yaml", m_txt_edit1->toPlainText().toStdString().c_str());
      return;
    }

    m_txt_edit->append(QString::fromStdString("Position: "+m_txt_edit1->toPlainText().toStdString()+" registered and saved correctly"));
    m_row_count++;
  }
  else
  {
    m_txt_edit->append(QString::fromStdString("Another position with the same name is already present. Chose a different name or Delete the old one"));
    ROS_ERROR("Another position with the same name is already present. Chose a different name or Delete the old one");
  }

}


void TeTeleop::registerPosition()
{
  //Store Position Name
  m_table_item = new QTableWidgetItem();
  if (m_txt_edit1->toPlainText().isEmpty())
  {
    m_table_item->setText(QString::fromStdString("Test"+std::to_string(m_row_count) ));
  }
  else
    m_table_item->setText(m_txt_edit1->toPlainText());
  m_table->insertRow(m_row_count);
  m_table->setItem(m_row_count,0,m_table_item);

  //Store Position Time
  std::time_t now = std::time(0);
  std::tm *ltm = localtime(&now);
  std::string date = std::to_string(ltm->tm_mday)+"/"+std::to_string(ltm->tm_mon+1)+","+std::to_string(ltm->tm_hour)+":"+std::to_string(ltm->tm_min);
  m_table_item2 = new QTableWidgetItem(QString::fromStdString(date));
  m_table->setItem(m_row_count,1,m_table_item2);

  return;
}



void TeTeleop::deletePosition()
{
  if (!m_table->selectedItems().isEmpty())
  {
    //Delete .YAML file
    std::string selected_pos = m_table->selectedItems().at(0)->text().toStdString();
    std::string string_to_save_yaml = "rm "+m_path_to_config+"/positions"+"/"+m_hi_namespace+"/"+selected_pos+".yaml";

    if (system( string_to_save_yaml.c_str() )!=0)
    {
      m_txt_edit->append(QString::fromStdString("Unable to delete "+selected_pos+".YAML file"));
      ROS_ERROR("Unable to delete %s.YAML file", selected_pos.c_str());
      return;
    }
    m_table->removeRow(m_table->selectedItems().at(0)->row());
    m_row_count = m_table->rowCount();
    m_txt_edit->append(QString::fromStdString("Position: "+selected_pos+" correctly deleted"));
  }
  else
    m_txt_edit->append(QString::fromStdString("No position selected to delete"));

}




void TeTeleop::moveToPosition()
{

  m_vmax_minimum= *std::min_element(m_vmax->begin(),m_vmax->end());

  m_msg_js.name.resize(m_nAx);
  m_msg_js.velocity.resize(m_nAx);
  m_msg_js.position.resize(m_nAx);

  if (!m_table->selectedItems().isEmpty())
  {
    if (m_button4->isDown())
    {
      //Load parameter only at first cycle
      if (!m_load)
      {
        //Load from selected .YAML to a parameter
        std::string string_to_save_yaml = "rosparam load "+m_path_to_config+"/positions"+"/"+m_hi_namespace+"/"+m_table->selectedItems().at(0)->text().toStdString()+".yaml /te_info_position";
        if (system( string_to_save_yaml.c_str() )!=0)
        {
          m_txt_edit->append(QString::fromStdString("Unable to store yaml parameters"));
          ROS_ERROR("Unable to store yaml parameters");
          return;
        }

        //Update target_pos
        if (!m_nh.getParam("/te_info_position/target_pos", m_target_pos))
        {
          m_txt_edit->append(QString::fromStdString("Parameter '/te_info_position/target_pos' does not exist"));
          ROS_ERROR("Parameter '/te_info_position/target_pos' does not exist");
          return;
        }
        m_load=true;
      }

      for (int i=0; i< m_nAx; i++)
      {
        m_msg_js.name.at(i)=m_controlled_joints->at(i);
        m_msg_js.velocity.at(i)=m_vmax_minimum*m_slider->value()/100.0;  //10% override as default, respect to the minimum velocity among
        m_msg_js.position.at(i)=m_target_pos.at(i);
      }
    }
    else
    {
      for (int i=0; i< m_nAx; i++)
      {
        m_msg_js.name.at(i)=m_controlled_joints->at(i);
        m_msg_js.velocity.at(i)=0.0;  //10% override as default
        m_msg_js.position.clear();
        m_load = false;
      }
    }
    m_msg_js.header.stamp=ros::Time::now();
    m_pub_js.publish(m_msg_js);
  }
  else
  {
    m_txt_edit->append(QString::fromStdString("No position selected to move"));
    return;
  }
}


void TeTeleop::loadPositions()
{
  std::string path_to_robots = m_path_to_config+"/positions/"+m_hi_namespace;
  std::vector<std::string> confs;

  //Locate directory /config
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (path_to_robots.c_str())) != NULL)
  {
    while ((ent = readdir (dir)) != NULL)
      if (strcmp(ent->d_name,".") && strcmp(ent->d_name,"..") )
        confs.push_back(ent->d_name);
    closedir (dir);
  }
  else
  {
    ROS_ERROR("Could not open directory %s: ", path_to_robots.c_str());
    m_txt_edit->append(QString::fromStdString("Could not open directory: "+path_to_robots));
    return;
  }

  for (int i=0; i<confs.size(); i++)
  {
    confs.at(i).erase(confs.at(i).end()-5,confs.at(i).end()); //Remove ".YAML" string at the end of file name

    //Check for confs with same names
    bool check = false;
    for (int j=0; j<m_table->rowCount(); j++)
    {
      if (!std::strcmp(confs.at(i).c_str(), m_table->item(j,0)->text().toStdString().c_str() ))
        check = true;;
    }

    if (!check)
    {
      //Load from selected .YAML to a parameter
      std::string string_to_save_yaml = "rosparam load "+m_path_to_config+"/positions"+"/"+m_hi_namespace+"/"+confs.at(i)+".yaml /te_move_position";
      if (system( string_to_save_yaml.c_str() )!=0)
      {
        m_txt_edit->append(QString::fromStdString("Unable to store yaml parameters\n"));
        ROS_ERROR("Unable to store yaml parameters");
        return;
      }

      //Update target_pos
      std::string time_data;
      if (!m_nh.getParam("/te_move_position/time_data", time_data))
      {
        m_txt_edit->append(QString::fromStdString("Parameter '/te_move_position/time_data' does not exist\n"));
        ROS_ERROR("Parameter '/te_move_position/time_data' does not exist");
        return;
      }

      //Print in table Position name
      m_table_item = new QTableWidgetItem();
      m_table_item->setText(QString::fromStdString(confs.at(i)));
      m_table->insertRow(m_row_count);
      m_table->setItem(m_row_count,0,m_table_item);

      //Print in table Position data
      m_table_item2 = new QTableWidgetItem(QString::fromStdString(time_data));
      m_table->setItem(m_row_count,1,m_table_item2);

      m_txt_edit->append(QString::fromStdString("Position: "+confs.at(i)+" correctly loaded"));
    }
  }


}






}


