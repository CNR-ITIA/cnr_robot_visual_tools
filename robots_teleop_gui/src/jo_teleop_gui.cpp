#include <jo_teleop_gui.h>


namespace joint_teleop_gui
{


JoTeleop::~JoTeleop()
{
}


JoTeleop::JoTeleop(basic_panel_gui::BasicPanel* basic_panel, std::string hi_namespace, std::string ctrl_namespace, std::vector<std::string>* controlled_joints, std::vector<joint_limits_interface::JointLimits> joints_limits)
{
  m_hi_namespace = hi_namespace;
  m_ctrl_namespace = ctrl_namespace;


  m_pub_js=m_nh.advertise<sensor_msgs::JointState>("/"+m_ctrl_namespace+"/target_joint_teleop",1);

  m_vmax = new std::vector<double>;
  for (int i=0; i < joints_limits.size(); i++)
    m_vmax->push_back(joints_limits.at(i).max_velocity);

  m_pmax = new std::vector<double>;
  for (int i=0; i < joints_limits.size(); i++)
    m_pmax->push_back(joints_limits.at(i).max_position);

  m_pmin = new std::vector<double>;
  for (int i=0; i < joints_limits.size(); i++)
    m_pmin->push_back(joints_limits.at(i).min_position);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_txt_edit = new QTextEdit();
  m_txt_edit = basic_panel->m_txt_edit;

  m_controlled_joints = new std::vector<std::string>;
  m_controlled_joints = controlled_joints;

  std::string feedback_joint_state_topic_name = "/"+m_hi_namespace+"/feedback_joint_state_topic";
  if( !m_nh.getParam("/"+m_hi_namespace+"/feedback_joint_state_topic", feedback_joint_state_topic_name))
  {
    m_txt_edit->append(QString::fromStdString("No parameter /"+m_hi_namespace+"/feedback_joint_state_topic defined in the configuration file."));
    ROS_ERROR("No parameter /%s/feedback_joint_state_topic defined in the configuration file.", m_hi_namespace.c_str());
  }

  m_joint_target_rec.reset(new ros_helper::SubscriptionNotifier<sensor_msgs::JointState>(m_nh,feedback_joint_state_topic_name, 1,boost::bind(&JoTeleop::callback,this,_1)));


  
  QString style_sheet="QGroupBox{border:1px solid gray;border-radius:5px;margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin;subcontrol-position:top center;padding:0 3px;}";  
  std::string path_to_images = ros::package::getPath("robots_teleop_gui");

  
  
  //Fill Tabs
  m_nAx = m_controlled_joints->size();
  m_button1.resize(m_nAx);
  m_button2.resize(m_nAx);
  m_button3.resize(m_nAx);
  m_txt_edit1.resize(m_nAx);
  m_slider_limits.resize(m_nAx);

  m_grid_layout1 = new QGridLayout();

  m_label2.resize(3);
  m_label2.at(0) = new QLabel();
  m_label2.at(0)->setText(QString::fromStdString("Controlled Joints"));
  m_grid_layout1->addWidget(m_label2.at(0),0,0,1,1);

  m_label2.at(1) = new QLabel();
  m_label2.at(1)->setText(QString::fromStdString("Pos"));
  m_grid_layout1->addWidget(m_label2.at(1),0,3,1,1);



  
  for (int i=0; i< m_nAx; i++)
  {     
    //First Tab Widgets
    m_label1 = new QLabel(); //TODO ritornarci
    m_label1->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
    m_label1->setText(QString::fromStdString(controlled_joints->at(i)));

    m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/minus.png"));
    m_button_icon = new QIcon(*m_pixmap);
    m_button1.at(i) = new QPushButton;
    m_button1.at(i)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
    m_button1.at(i)->setIcon(*m_button_icon);
    m_button1.at(i)->setAutoRepeat(true);


    m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/plus.png"));
    m_button_icon = new QIcon(*m_pixmap);
    m_button2.at(i) = new QPushButton;
    m_button2.at(i)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
    m_button2.at(i)->setIcon(*m_button_icon);
    m_button2.at(i)->setIconSize(m_pixmap->rect().size());
    m_button2.at(i)->setAutoRepeat(true);


    m_grid_layout1->addWidget(m_label1,1+i,0,1,1);
    m_grid_layout1->addWidget(m_button1.at(i),1+i,1,1,1);
    m_grid_layout1->addWidget(m_button2.at(i),1+i,2,1,1);

  
    //Second Tab Widgets

    m_txt_edit1.at(i) = new QTextEdit;
    m_txt_edit1.at(i)->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
    m_txt_edit1.at(i)->setFixedWidth(50);
    m_txt_edit1.at(i)->setFixedHeight(m_txt_edit1.at(i)->contentsMargins().top()+m_txt_edit1.at(i)->contentsMargins().bottom()+m_txt_edit1.at(i)->document()->size().height());
    m_txt_edit1.at(i)->setText(QString::fromStdString("0.0"));

    m_label3 = new QLabel(); //TODO ritornarci
    m_label3->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
    m_label3->setText(QString::fromStdString("[rad]"));
    m_label3->setFixedWidth(40);


    m_slider_limits.at(i) = new QSlider(Qt::Horizontal,0);
    m_slider_limits.at(i)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
    m_slider_limits.at(i)->setRange(m_pmin->at(i)*100,m_pmax->at(i)*100);
    m_slider_limits.at(i)->setValue(0.0);
    m_slider_limits.at(i)->setTickInterval(100);
    m_slider_limits.at(i)->setStyleSheet("QSlider::groove:horizontal { "
                                         "border: 1px solid #999999; "
                                         "height: 15px; "
                                         "background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #B1B1B1, stop:1 #c4c4c4); "
                                         "margin: 2px 0; "
                                         "} "
                                         "QSlider::handle:horizontal { "
                                         "background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #f26161, stop:1 #ff2B2B); "
                                         "border: 1px solid #5c5c5c; "
                                         "width: 10px; "
                                         "margin: -2px 0px; "
                                         "} ");

    m_button3.at(i) = new QPushButton;
    m_button3.at(i)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
    m_button3.at(i)->setText("Move");
    m_button3.at(i)->setAutoRepeat(true);

    m_grid_layout1->addWidget(m_slider_limits.at(i),1+i,3,1,1);
    m_grid_layout1->addWidget(m_txt_edit1.at(i),1+i,4,1,1);
    m_grid_layout1->addWidget(m_label3,1+i,5,1,1);
    m_grid_layout1->addWidget(m_button3.at(i),1+i,6,1,-1);
  }


  for (int i=0; i<m_nAx; i++)
  {
    connect(m_button1.at(i), SIGNAL(pressed()), this, SLOT(regulateVelocity()));
    connect(m_button1.at(i), SIGNAL(released()), this, SLOT(regulateVelocity()));
    connect(m_button2.at(i), SIGNAL(pressed()), this, SLOT(regulateVelocity()));
    connect(m_button2.at(i), SIGNAL(released()), this, SLOT(regulateVelocity()));
    connect(m_button3.at(i), SIGNAL(pressed()), this, SLOT(moveJointPrecision()));
    connect(m_button3.at(i), SIGNAL(released()), this, SLOT(moveJointPrecision()));
  }
  
}

void JoTeleop::callback(const sensor_msgs::JointStateConstPtr msg)
{
  for (int i=0; i<m_nAx; i++)
    m_slider_limits.at(i)->setValue(msg->position.at(i)*100);
}

//Joint Teleop Methods
void JoTeleop::regulateVelocity()
{
  m_msg_js.name.resize(1);
  m_msg_js.velocity.resize(1);
  m_msg_js.position.clear();

  bool repeat=false;

  for (int i=0; i< m_nAx; i++)
  {
    if (m_button1.at(i)->isDown())
    {
      m_msg_js.name.at(0)=m_controlled_joints->at(i);
      m_msg_js.velocity.at(0)=-1*m_vmax->at(i)*m_slider->value()/100.0;
      repeat=true;
    }
    else if (m_button2.at(i)->isDown())
    {
      m_msg_js.name.at(0)=m_controlled_joints->at(i);
      m_msg_js.velocity.at(0)=m_vmax->at(i)*m_slider->value()/100.0;
      repeat=true;
    }
  }
    if (!repeat)
      m_msg_js.velocity.at(0)=0.0;

  m_msg_js.header.stamp=ros::Time::now();
  m_pub_js.publish(m_msg_js);
}



void JoTeleop::moveJointPrecision()
{
  m_msg_js.name.resize(1);
  m_msg_js.velocity.resize(1);
  m_msg_js.position.resize(1);

  bool repeat=false;
  for (int i=0; i< m_nAx; i++)
    if (m_button3.at(i)->isDown())
    {
      m_msg_js.name.at(0)=m_controlled_joints->at(i);
      m_msg_js.velocity.at(0)=m_vmax->at(i)*m_slider->value()/100;  //10% override as default
      m_msg_js.position.at(0)=m_txt_edit1.at(i)->toPlainText().toDouble();
      repeat=true;
    }
  if (!repeat)
  {
    m_msg_js.velocity.at(0)=0.0;
    m_msg_js.position.clear();
  }
  m_msg_js.header.stamp=ros::Time::now();
  m_pub_js.publish(m_msg_js);
}



}



