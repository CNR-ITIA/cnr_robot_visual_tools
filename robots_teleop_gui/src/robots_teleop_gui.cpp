#include <robots_teleop_gui.h>
#include <pluginlib/class_list_macros.h>


PLUGINLIB_EXPORT_CLASS(robots_teleop_gui::RobotsTeleop,rviz::Panel)


namespace robots_teleop_gui
{

  RobotsTeleop::RobotsTeleop(QWidget* parent) : Panel(parent)
  {
  }


  RobotsTeleop::~RobotsTeleop()
  {
  }


  void RobotsTeleop::onInitialize()
  {

    //Find RobotHardware with Teleop Controller associated
    m_srv_client_start = m_nh.serviceClient<configuration_msgs::StartConfiguration>("/configuration_manager/start_configuration");

    ros::ServiceClient srv_client = m_nh.serviceClient<configuration_msgs::ListConfigurations>("/configuration_manager/list_configurations");
    configuration_msgs::ListConfigurations srv;
    srv_client.call(srv);

    ROS_DEBUG_STREAM( "Active configurations: " << srv.response );

    std::vector<std::string> jc_conf; //Configurations name
    std::vector<std::string> jc_hi; //HI name
    std::vector<std::string> jc_ctrl; //Controllers name
    std::vector<std::string> jc_hi_ctrl;  //Hi-Controllers name

    for (size_t i=0; i<srv.response.configurations.size(); i++)
    {
      for (size_t j=0; j<srv.response.configurations.at(i).hardware_interfaces.size(); j++)
      {
        std::string tmp_hi_ctrl = srv.response.configurations.at(i).hardware_interfaces.at(j) + "/" +srv.response.configurations.at(i).controllers.at(j);

        if (std::find(jc_hi_ctrl.begin(), jc_hi_ctrl.end(), tmp_hi_ctrl.c_str()) == jc_hi_ctrl.end()) //Return a pointer to last value if condition is not satisfied
        {
          jc_conf.push_back(srv.response.configurations.at(i).name);
          jc_hi.push_back(srv.response.configurations.at(i).hardware_interfaces.at(j));
          jc_ctrl.push_back(srv.response.configurations.at(i).controllers.at(j));
          jc_hi_ctrl.push_back(jc_hi.back()+"/"+jc_ctrl.back());
        }
      }
    }

    ROS_INFO("Loading the Teleop Hardware Resource");
    for (size_t i=0; i<jc_hi_ctrl.size(); i++)
    {
      std::string tmp_ctrl_types;
      if (!m_nh.getParam("/" + jc_hi_ctrl.at(i)+"/type", tmp_ctrl_types))
      {
        if (!m_nh.getParam(jc_hi_ctrl.at(i)+"/type", tmp_ctrl_types))
        {
          ROS_ERROR(">>> '%s' of type '%s is not in rosparam server' ",
                        std::string( jc_hi_ctrl.at(i)+"/type").c_str(), tmp_ctrl_types.c_str() );
          continue;
        }
      }

      ROS_INFO(">>> Check '%s' of type '%s' ", std::string( jc_hi_ctrl.at(i)+"/type").c_str(), tmp_ctrl_types.c_str() );
      if (( strcmp(tmp_ctrl_types.c_str(), "itia/control/JointTeleopController")==0) || (strcmp(tmp_ctrl_types.c_str(), "itia/control/CartTeleopController")==0 )
      ||(strcmp(tmp_ctrl_types.c_str(), "cnr/control/JointTeleopController")==0) || (strcmp(tmp_ctrl_types.c_str(), "cnr/control/CartesianTeleopController")==0))
      {
        m_jc2_conf.push_back(jc_conf.at(i));
        m_jc2_hi.push_back(jc_hi.at(i));
        m_jc2_ctrl.push_back(jc_ctrl.at(i));
        m_jc2_hi_ctrl.push_back(jc_hi.at(i)+"/"+jc_ctrl.at(i));
        m_jc2_hi_ctrl_types.push_back(tmp_ctrl_types);

        if (std::find(m_jc_hi.begin(), m_jc_hi.end(), jc_hi.at(i).c_str()) == m_jc_hi.end())
        {
          m_jc_hi.push_back(jc_hi.at(i)); //Hardware Interfaces with all Teleop controllers and without repetitions
          ROS_INFO_STREAM("<<< Added a new Teleop controller '" << jc_hi.at(i)<<"'");
        }
      }
      else
      {
        ROS_INFO_STREAM("<<< Skip '" << jc_hi.at(i)<<"'");
      }
    }





    m_bool1.resize(m_jc_hi.size());
    m_bool2.resize(m_jc_hi.size());
    m_bool1_hi.resize(m_jc_hi.size());
    m_bool1_conf.resize(m_jc_hi.size());
    m_bool2_conf.resize(m_jc_hi.size());


    for (size_t i=0; i<m_jc_hi.size(); i++)
      for (size_t j=0; j<m_jc2_hi.size(); j++)
        if (strcmp(m_jc_hi.at(i).c_str(),m_jc2_hi.at(j).c_str())==0)
        {
          if ((strcmp(m_jc2_hi_ctrl_types.at(j).c_str(),"itia/control/JointTeleopController")==0)
          ||(strcmp(m_jc2_hi_ctrl_types.at(j).c_str(),"cnr/control/JointTeleopController")==0))
          {
            m_bool1.at(i) = m_jc2_hi_ctrl.at(j);
            m_bool1_hi.at(i) = m_jc2_hi.at(j);
            m_bool1_conf.at(i) = m_jc2_conf.at(j);
          }
          else
          {
            m_bool2.at(i) = m_jc2_hi_ctrl.at(j);  //For the "itia/control/CartTeleopController"
                                                  //or "cnr/control/CartesianTeleopController"
            m_bool2_conf.at(i) = m_jc2_conf.at(j);
          }
        }


    std::string robots_description;
    if (!m_nh.getParam("/robot_description", robots_description))
    {
      ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
      return;
    }
    m_robot_model = urdf::parseURDF(robots_description);


    m_tab1 = new QTabWidget();
    for (unsigned int idx=0; idx < m_jc_hi.size(); idx++)
    {
      m_controlled_joints = new std::vector<std::string>;
      m_vmax = new std::vector<double>;
      m_pmax = new std::vector<double>;
      m_pmin = new std::vector<double>;


      if (!m_nh.getParam("/"+m_jc_hi.at(idx)+"/joint_names/", *m_controlled_joints))
      {
        ROS_FATAL_STREAM("Unable to load parameter: /"+m_jc_hi.at(idx)+"/joint_names/");
        return;
      }

      m_nAx = m_controlled_joints->size();
      m_joints_limits.clear();
      m_joints_limits.resize(m_nAx);

      //Check if all controlled_joints are defined in /robots_description URDF and check correct Type
      for (int iAx=0; iAx<m_nAx; iAx++)
      {
        m_check1 = false;
        for (const std::pair<std::string, urdf::JointSharedPtr>& joint_tuple: m_robot_model->joints_)
          if ((joint_tuple.second->type==urdf::Joint::REVOLUTE) || (joint_tuple.second->type==urdf::Joint::PRISMATIC))
          {
            if (!m_controlled_joints->at(iAx).compare(joint_tuple.first))
            {
              m_pmin->push_back(joint_tuple.second->limits->lower); //posizione minima
              m_pmax->push_back(joint_tuple.second->limits->upper);  //posizone massima
              m_vmax->push_back(joint_tuple.second->limits->velocity);  //velocitÃ  massima

              m_joints_limits.at(iAx).min_position = joint_tuple.second->limits->lower;
              m_joints_limits.at(iAx).max_position = joint_tuple.second->limits->upper;
              m_joints_limits.at(iAx).max_velocity = joint_tuple.second->limits->velocity;

              m_check1 = true;
            }
          }
        if (!m_check1)
        {
          ROS_ERROR("The joint named %s in /%s is not defined in /robots_description. Definition in .urdf is missing or is not type REVOLUTE or PRISMATIC", m_controlled_joints->at(iAx).c_str(),  m_jc_hi.at(idx).c_str());
          return;
        }
      }

      m_w1 = new QWidget();
      m_w2 = new QWidget();
      m_w3 = new QWidget();

      //Main Layout
      m_bp = new basic_panel_gui::BasicPanel();


      if (strcmp(m_bool1.at(idx).c_str(),"")!=0)
      {
        //Joints Teleop
        m_jt = new joint_teleop_gui::JoTeleop(m_bp, m_bool1_hi.at(idx).c_str(), m_bool1.at(idx).c_str(), m_controlled_joints, m_joints_limits);
        m_w2->setLayout(m_jt->m_grid_layout1);

        //Teach Teleop
        m_tt = new teach_teleop_gui::TeTeleop(m_bp, m_bool1_hi.at(idx).c_str(), m_bool1.at(idx).c_str(), m_controlled_joints, m_joints_limits);
        m_w3->setLayout(m_tt->m_grid_layout4);
      }
      else
        m_bp->m_txt_edit->setText(QString::fromStdString("No controller of type [cnr|itia]/control/JointTeleopController defined for namespace /"+m_jc_hi.at(idx)+". Add a proper controller."));


      if (strcmp(m_bool2.at(idx).c_str(),"")!=0)
      {
        //Cartesian Teleop
        m_ct = new cart_teleop_gui::CaTeleop(m_bp, m_bool2.at(idx).c_str());
        m_w1->setLayout(m_ct->m_grid_layout1);
      }
      else
        m_bp->m_txt_edit->setText(QString::fromStdString("No controller of type [cnr|itia]/control/CartTeleopController defined for namespace /"+m_jc_hi.at(idx)+". Add a proper controller."));

      m_tab2 = new QTabWidget();
      m_tab2->addTab(m_w1, "Cartesian Movement");
      m_tab2->addTab(m_w2, "Joint Movement");
      m_tab2->addTab(m_w3, "Teach Position");

      m_grid_layout3 = new QVBoxLayout();
      m_grid_layout3->addWidget(m_tab2);

      m_grid_layout2 = new QVBoxLayout();
      m_grid_layout2->addLayout(m_grid_layout3);
      m_grid_layout2->addLayout(m_bp->m_layout7);

      m_w4 = new QWidget();
      m_w4->setLayout(m_grid_layout2);
      m_tab1->addTab(m_w4,QString::fromStdString(m_jc_hi.at(idx)) );

    }

    m_grid_layout1 = new QVBoxLayout(this);
    m_grid_layout1->addWidget(m_tab1);

    m_button = new QPushButton();
    m_grid_layout1->addWidget(m_button);

    if(m_tab2)
    {
      connect(m_tab2, SIGNAL(currentChanged(int)), this, SLOT(changeConfigurations())); //int needeed
    }
    else
    {
      ROS_WARN("None tab is created, since it seems that there are not teleop controller listed in rosparam server.");
    }


  }


  void RobotsTeleop::changeConfigurations()
  {
    m_srv_start.request.strictness = 1;

    if(m_tab2->currentIndex()==0)
      m_srv_start.request.start_configuration = m_bool2_conf.at(0).c_str();
    else
      m_srv_start.request.start_configuration = m_bool1_conf.at(0).c_str();

    m_srv_client_start.call(m_srv_start);
  }

}









