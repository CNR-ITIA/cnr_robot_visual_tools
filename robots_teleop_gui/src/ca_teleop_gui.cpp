#include <ca_teleop_gui.h>


namespace cart_teleop_gui
{



DriveWidget::DriveWidget(basic_panel_gui::BasicPanel* basic_panel, std::string ctrl_namespace, QComboBox* dropdown_menu, std::vector<QPushButton*> button)
{

  m_pub_ts=m_nh.advertise<geometry_msgs::TwistStamped>("/"+ctrl_namespace+"/target_cart_teleop",1);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_dropdown_menu = new QComboBox();
  m_dropdown_menu = dropdown_menu;

  m_frame_button = button;

  m_timer = new QTimer();
  connect(m_timer, SIGNAL(timeout()), this, SLOT(publishTwist()));




}
void DriveWidget::publishTwist()
{

    if (m_frame_button.at(0)->isChecked())
      m_msg_ts.header.frame_id = "BASE";
    else if (m_frame_button.at(1)->isChecked())
      m_msg_ts.header.frame_id = "TOOL";

    if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XY"))
    {
      m_msg_ts.twist.linear.x = m_slider->value()/100.0*2.5*m_mouse_x/2;  // Divided by 2 to limit speed
      m_msg_ts.twist.linear.y = m_slider->value()/100.0*2.5*m_mouse_y/2;
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XZ"))
    {
      m_msg_ts.twist.linear.x = m_slider->value()/100.0*2.5*m_mouse_x/2;
      m_msg_ts.twist.linear.z = m_slider->value()/100.0*2.5*m_mouse_y/2;
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane YZ"))
    {
      m_msg_ts.twist.linear.y = m_slider->value()/100.0*2.5*m_mouse_x/2;
      m_msg_ts.twist.linear.z = m_slider->value()/100.0*2.5*m_mouse_y/2;
    }

    m_pub_ts.publish(m_msg_ts);

}

void DriveWidget::paintEvent(QPaintEvent *event)
{
    QColor background = Qt::gray;
    int w = width();
    int h = height();
    m_size = (( w > h ) ? h : w) - 1;
    m_hpad = ( w - m_size ) / 2;
    m_vpad = ( h - m_size ) / 2;

    m_painter = new QPainter(this);
    m_painter->setBrush( background );
    m_painter->setPen(Qt::black);
    m_painter->drawRect( QRect( m_hpad, m_vpad, m_size, m_size));



    if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XY"))
    {
        m_painter->setPen(Qt::red);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::green);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane XZ"))
    {
        m_painter->setPen(Qt::red);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::blue);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }
    else if (!std::strcmp(m_dropdown_menu->itemText(m_dropdown_menu->currentIndex()).toStdString().c_str(), "Plane YZ"))
    {
        m_painter->setPen(Qt::green);
        m_painter->drawLine( m_hpad, height() / 2, m_hpad + m_size, height() / 2 );
        m_painter->setPen(Qt::blue);
        m_painter->drawLine( width() / 2, m_vpad, width() / 2, m_vpad + m_size );
    }

    m_painter->end();
}


void DriveWidget::mouseMoveEvent( QMouseEvent* event )
{
    m_mouse_x = m_hpad-event->x();
    m_mouse_x = -(m_hpad-event->x() + m_size/2);
    m_mouse_x =  m_mouse_x / (m_size/2);

    if (m_mouse_x > 1 || m_mouse_x < -1)
      m_mouse_x = 0.0;

    m_mouse_y = m_vpad-event->y();
    m_mouse_y = (m_vpad-event->y() + m_size/2);
    m_mouse_y =  m_mouse_y / (m_size/2);

    if (m_mouse_y > 1 || m_mouse_y < -1)
      m_mouse_y = 0.0;

}

void DriveWidget::mousePressEvent( QMouseEvent* event )
{

    m_mouse_x = m_hpad-event->x();
    m_mouse_x = -(m_hpad-event->x() + m_size/2);
    m_mouse_x =  m_mouse_x / (m_size/2);

    if (m_mouse_x > 1 || m_mouse_x < -1)
      m_mouse_x = 0.0;

    m_mouse_y = m_vpad-event->y();
    m_mouse_y = (m_vpad-event->y() + m_size/2);
    m_mouse_y =  m_mouse_y / (m_size/2);

    if (m_mouse_y > 1 || m_mouse_y < -1)
      m_mouse_y = 0.0;

    m_timer->start(100); //time specified in ms
}

void DriveWidget::mouseReleaseEvent( QMouseEvent* event )
{
    m_timer->stop();
    m_mouse_x = 0.0;
    m_mouse_y = 0.0;

    m_msg_ts.twist.linear.x = 0.0;
    m_msg_ts.twist.linear.y = 0.0;
    m_msg_ts.twist.linear.z = 0.0;
    m_pub_ts.publish(m_msg_ts);

}







CaTeleop::~CaTeleop()
{
}

CaTeleop::CaTeleop(basic_panel_gui::BasicPanel* basic_panel, std::string ctrl_namespace)
{
  m_pub_ts=m_nh.advertise<geometry_msgs::TwistStamped>("/"+ctrl_namespace+"/target_cart_teleop",1);

  m_slider = new QSlider();
  m_slider = basic_panel->m_slider;

  m_txt_edit = new QTextEdit();
  m_txt_edit = basic_panel->m_txt_edit;

  m_button1.resize(2);
  m_button2.resize(2);
  m_button3.resize(2);
  m_button4.resize(2);
  m_button5.resize(2);
  m_button6.resize(2);
  m_button7.resize(2);

  //FRONT, BACK, LEFT, RIGHT
  QString style_sheet="QGroupBox{border:1px solid gray;border-radius:5px;margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin;subcontrol-position:top center;padding:0 3px;}";
  std::string path_to_images = ros::package::getPath("robots_teleop_gui");

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/up_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button1.at(0) = new QPushButton;
  m_button1.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button1.at(0)->setIcon(*m_button_icon);
  m_button1.at(0)->setIconSize(m_pixmap->rect().size());
  m_button1.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/down_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button1.at(1) = new QPushButton;
  m_button1.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button1.at(1)->setIcon(*m_button_icon);
  m_button1.at(1)->setIconSize(m_pixmap->rect().size());
  m_button1.at(1)->setAutoRepeat(true);

  m_grid_layout3 = new QGridLayout();
  m_grid_layout3->addWidget(m_button1.at(0),1,2,1,1);
  m_grid_layout3->addWidget(m_button1.at(1),3,2,1,1);


  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/right_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button2.at(0) = new QPushButton;
  m_button2.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button2.at(0)->setIcon(*m_button_icon);
  m_button2.at(0)->setIconSize(m_pixmap->rect().size());
  m_button2.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/left_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button2.at(1) = new QPushButton;
  m_button2.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button2.at(1)->setIcon(*m_button_icon);
  m_button2.at(1)->setIconSize(m_pixmap->rect().size());
  m_button2.at(1)->setAutoRepeat(true);

  m_grid_layout3->addWidget(m_button2.at(0),2,3,1,1);
  m_grid_layout3->addWidget(m_button2.at(1),2,1,1,1);


  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/y+_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button3.at(0) = new QPushButton;
  m_button3.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button3.at(0)->setIcon(*m_button_icon);
  m_button3.at(0)->setIconSize(m_pixmap->rect().size());
  m_button3.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/y-_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button3.at(1) = new QPushButton;
  m_button3.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button3.at(1)->setIcon(*m_button_icon);
  m_button3.at(1)->setIconSize(m_pixmap->rect().size());
  m_button3.at(1)->setAutoRepeat(true);

  m_grid_layout3->addWidget(m_button3.at(0),0,2,1,1);
  m_grid_layout3->addWidget(m_button3.at(1),4,2,1,1);


  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/x-_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button4.at(0) = new QPushButton;
  m_button4.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button4.at(0)->setIcon(*m_button_icon);
  m_button4.at(0)->setIconSize(m_pixmap->rect().size());
  m_button4.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/x+_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button4.at(1) = new QPushButton;
  m_button4.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button4.at(1)->setIcon(*m_button_icon);
  m_button4.at(1)->setIconSize(m_pixmap->rect().size());
  m_button4.at(1)->setAutoRepeat(true);

  m_grid_layout3->addWidget(m_button4.at(0),2,0,1,1);
  m_grid_layout3->addWidget(m_button4.at(1),2,4,1,1);


  m_button7.at(0) = new QPushButton;
  m_button7.at(0)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button7.at(0)->setAutoRepeat(true);
  m_button7.at(0)->setText("Base frame");
  m_button7.at(0)->setCheckable(true);
  m_button7.at(0)->setChecked(true);
  m_msg_ts.header.frame_id="BASE"; //If no frame is initially checked, default is BASE

  m_button7.at(1) = new QPushButton;
  m_button7.at(1)->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_button7.at(1)->setAutoRepeat(true);
  m_button7.at(1)->setText("Tool frame");
  m_button7.at(1)->setCheckable(true);
  m_button7.at(1)->setChecked(false);

  m_grid_layout3->addWidget(m_button7.at(0),0,3,1,-1);
  m_grid_layout3->addWidget(m_button7.at(1),1,3,1,-1);

  m_group1 = new QGroupBox();
  m_group1->setTitle("XY-Plane");
  m_group1->setStyleSheet(style_sheet);
  m_group1->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_group1->setLayout(m_grid_layout3);

  m_grid_layout1 = new QGridLayout();
  m_grid_layout1->addWidget(m_group1,0,0,1,1);



  //UP AND DOWN
  m_grid_layout4 = new QGridLayout();

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/up_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button5.at(0) = new QPushButton;
  m_button5.at(0)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_button5.at(0)->setIcon(*m_button_icon);
  m_button5.at(0)->setIconSize(m_pixmap->rect().size());
  m_button5.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/down_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button5.at(1) = new QPushButton;
  m_button5.at(1)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_button5.at(1)->setIcon(*m_button_icon);
  m_button5.at(1)->setIconSize(m_pixmap->rect().size());
  m_button5.at(1)->setAutoRepeat(true);

  m_grid_layout4->addWidget(m_button5.at(0),1,0,1,1);
  m_grid_layout4->addWidget(m_button5.at(1),3,0,1,1);


  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/y+_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button6.at(0) = new QPushButton;
  m_button6.at(0)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_button6.at(0)->setIcon(*m_button_icon);
  m_button6.at(0)->setIconSize(m_pixmap->rect().size());
  m_button6.at(0)->setAutoRepeat(true);

  m_pixmap = new QPixmap(QString::fromStdString(path_to_images+"/images/y-_arrow.png"));
  m_button_icon = new QIcon(*m_pixmap);
  m_button6.at(1) = new QPushButton;
  m_button6.at(1)->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_button6.at(1)->setIcon(*m_button_icon);
  m_button6.at(1)->setIconSize(m_pixmap->rect().size());
  m_button6.at(1)->setAutoRepeat(true);

  m_grid_layout4->addWidget(m_button6.at(0),0,0,1,1);
  m_grid_layout4->addWidget(m_button6.at(1),4,0,1,1);


  m_spacer = new  QSpacerItem(0,0,QSizePolicy::Expanding,QSizePolicy::Expanding);;
  m_grid_layout4->addItem(m_spacer,2,0,1,1);

  m_group3 = new QGroupBox();
  m_group3->setTitle("Z-Axis");
  m_group3->setLayout(m_grid_layout4);
  m_group3->setStyleSheet(style_sheet);
  m_group3->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Expanding);
  m_grid_layout1->addWidget(m_group3,0,1,1,1);


  //FREE MOVEMENT
  m_dropdown_menu = new QComboBox();
  m_dropdown_menu->addItem("Plane XY");
  m_dropdown_menu->addItem("Plane XZ");
  m_dropdown_menu->addItem("Plane YZ");


  m_layout5 = new QVBoxLayout();
  m_layout5->addWidget(m_dropdown_menu);

  m_drive_widget = new DriveWidget(basic_panel, ctrl_namespace, m_dropdown_menu, m_button7);
  m_layout5->addWidget(m_drive_widget);

  m_group4 = new QGroupBox();
  m_group4->setTitle("Free Move");
  m_group4->setStyleSheet(style_sheet);
  m_group4->setLayout(m_layout5);

  m_grid_layout1->addWidget(m_group4,0,2,1,-1);

//  //TOPIC BOX
//  m_txt_edit2 = new QTextEdit();
//  m_txt_edit2->setFixedHeight(m_dropdown_menu->height());
//  m_layout6 = new QVBoxLayout();
//  m_layout6->addWidget(m_txt_edit2);
//  m_group5 = new QGroupBox();
//  m_group5->setTitle("Free Move");
//  m_group5->setStyleSheet(style_sheet);
//  m_group5->setLayout(m_layout6);

//  m_grid_layout1->addWidget(m_group5,0,2,1,-1);



  connect(m_button1.at(0), SIGNAL(pressed()), this, SLOT(moveY()));
  connect(m_button1.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button1.at(1), SIGNAL(pressed()), this, SLOT(moveY()));
  connect(m_button1.at(1), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button2.at(0), SIGNAL(pressed()), this, SLOT(moveX()));
  connect(m_button2.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button2.at(1), SIGNAL(pressed()), this, SLOT(moveX()));
  connect(m_button2.at(1), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button3.at(0), SIGNAL(pressed()), this, SLOT(rotateY()));
  connect(m_button3.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button3.at(1), SIGNAL(pressed()), this, SLOT(rotateY()));
  connect(m_button3.at(1), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button4.at(0), SIGNAL(pressed()), this, SLOT(rotateX()));
  connect(m_button4.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button4.at(1), SIGNAL(pressed()), this, SLOT(rotateX()));
  connect(m_button4.at(1), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button7.at(0), SIGNAL(clicked()), this, SLOT(selectBaseFrame()));
  connect(m_button7.at(1), SIGNAL(clicked()), this, SLOT(selectToolFrame()));
  connect(m_button5.at(0), SIGNAL(pressed()), this, SLOT(moveZ()));
  connect(m_button5.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button5.at(1), SIGNAL(pressed()), this, SLOT(moveZ()));
  connect(m_button5.at(1), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button6.at(0), SIGNAL(pressed()), this, SLOT(rotateZ()));
  connect(m_button6.at(0), SIGNAL(released()), this, SLOT(resetTwist()));
  connect(m_button6.at(1), SIGNAL(pressed()), this, SLOT(rotateZ()));
  connect(m_button6.at(1), SIGNAL(released()), this, SLOT(resetTwist()));

  connect(m_dropdown_menu, SIGNAL(activated(int)), m_drive_widget, SLOT(update()));


}




//Cart Teleop Methods
void CaTeleop::moveX()
{
  setTwistZero();
  if (m_button2.at(0)->isDown())
    m_msg_ts.twist.linear.x = m_slider->value()/100.0*0.25;
  else
    m_msg_ts.twist.linear.x = -m_slider->value()/100.0*0.25;


  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::moveY()
{
  setTwistZero();
  if (m_button1.at(0)->isDown())
    m_msg_ts.twist.linear.y = m_slider->value()/100.0*0.25;
  else
    m_msg_ts.twist.linear.y = -m_slider->value()/100.0*0.25;

  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::moveZ()
{
  setTwistZero();
  if (m_button5.at(0)->isDown())
    m_msg_ts.twist.linear.z = m_slider->value()/100.0*0.25;
  else
    m_msg_ts.twist.linear.z = -m_slider->value()/100.0*0.25;

  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::rotateX()
{
  setTwistZero();
  if (m_button4.at(0)->isDown())
    m_msg_ts.twist.angular.x = m_slider->value()/100.0*1;
  else
    m_msg_ts.twist.angular.x = -m_slider->value()/100.0*1;

  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::rotateY()
{
  setTwistZero();
  if (m_button3.at(0)->isDown())
    m_msg_ts.twist.angular.y = m_slider->value()/100.0*1;
  else
    m_msg_ts.twist.angular.y = -m_slider->value()/100.0*1;

  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::rotateZ()
{
  setTwistZero();
  if (m_button6.at(0)->isDown())
    m_msg_ts.twist.angular.z = m_slider->value()/100.0*1;
  else
    m_msg_ts.twist.angular.z = -m_slider->value()/100.0*1;

  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}

void CaTeleop::resetTwist()
{
  setTwistZero();
  m_msg_ts.header.stamp=ros::Time::now();
  m_pub_ts.publish(m_msg_ts);
}



void CaTeleop::setTwistZero()
{
  m_msg_ts.twist.linear.x = 0.0;
  m_msg_ts.twist.linear.y = 0.0;
  m_msg_ts.twist.linear.z = 0.0;
  m_msg_ts.twist.angular.x = 0.0;
  m_msg_ts.twist.angular.y = 0.0;
  m_msg_ts.twist.angular.z = 0.0;
}

void CaTeleop::selectBaseFrame()
{
  m_msg_ts.header.frame_id="BASE";
  m_button7.at(1)->setChecked(false);
  m_button7.at(0)->setChecked(true);
}

void CaTeleop::selectToolFrame()
{
  m_msg_ts.header.frame_id="TOOL";
  m_button7.at(0)->setChecked(false);
  m_button7.at(1)->setChecked(true);
}







}





