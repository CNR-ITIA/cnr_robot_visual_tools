#include <basic_panel_gui.h>


namespace basic_panel_gui
{

BasicPanel::~BasicPanel()
{
}

BasicPanel::BasicPanel()
{

  QString style_sheet="QGroupBox{border:1px solid gray;border-radius:5px;margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin;subcontrol-position:top center;padding:0 3px;}";

  //Override
  m_slider = new QSlider(Qt::Horizontal,0);
  m_slider->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Maximum);
  m_slider->setRange(0,100);
  m_slider->setValue(10);

  m_label = new QLabel();
  m_label->setSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum);
  m_label->setText(QString::fromStdString("10%"));

  m_layout5 = new QHBoxLayout();
  m_layout5->addWidget(m_label);
  m_layout5->addWidget(m_slider);

  m_group1 = new QGroupBox();
  m_group1->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Maximum);
  m_group1->setTitle("Override velocity");
  m_group1->setStyleSheet(style_sheet);
  m_group1->setLayout(m_layout5);


  m_layout7 = new QVBoxLayout();
  m_layout7->addWidget(m_group1);


  //Info Box
  m_txt_edit = new QTextEdit();
  m_txt_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_txt_edit->setMinimumHeight(2*m_txt_edit->document()->size().height()+m_txt_edit->contentsMargins().top()+m_txt_edit->contentsMargins().bottom());
  m_txt_edit->setReadOnly(false);

  m_layout6 = new QVBoxLayout;
  m_layout6->addWidget(m_txt_edit);

  m_group2 = new QGroupBox();
//  m_group2->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
  m_group2->setTitle("Info");
  m_group2->setStyleSheet(style_sheet);
  m_group2->setLayout(m_layout6);

  m_layout7->addWidget(m_group2);

  connect(m_slider, SIGNAL(valueChanged(int)),this, SLOT(overrideVelocity()));


}

void BasicPanel::overrideVelocity()
{
  m_label->setText(QString::fromStdString(std::to_string(m_slider->value())+"%"));
}



}




