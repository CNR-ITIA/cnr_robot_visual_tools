#ifndef robots_teleop_gui_201812101007
#define robots_teleop_gui_201812101007

#include <ros/ros.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include <dirent.h>

#include <sensor_msgs/JointState.h>
#include <geometry_msgs/TwistStamped.h>
#include <diagnostic_msgs/KeyValue.h>
#include <subscription_notifier/subscription_notifier.h>
#include <joint_limits_interface/joint_limits.h>
#include <configuration_msgs/ListConfigurations.h>
#include <configuration_msgs/StartConfiguration.h>
#include <configuration_msgs/StopConfiguration.h>


#include "rviz/panel.h"

#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <QSpacerItem>
#include <QTabWidget>
#include <QWidget>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QComboBox>
#include <QTextEdit>

#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>

#include "basic_panel_gui.h"
#include "ca_teleop_gui.h"
#include "jo_teleop_gui.h"
#include "te_teleop_gui.h"


namespace robots_teleop_gui
{
  class RobotsTeleop: public rviz::Panel
  {
    
  Q_OBJECT

  public Q_SLOTS:
    void changeConfigurations();


  public:
    RobotsTeleop( QWidget* parent = 0 );
    virtual ~RobotsTeleop();
    virtual void onInitialize();
    /** @brief Load the given html file. */
    
   

//     
    
  protected:
    basic_panel_gui::BasicPanel* m_bp = nullptr;
    cart_teleop_gui::CaTeleop* m_ct = nullptr;
    joint_teleop_gui::JoTeleop* m_jt= nullptr;
    teach_teleop_gui::TeTeleop* m_tt = nullptr;

    QVBoxLayout* m_grid_layout1 = nullptr;
    QVBoxLayout* m_grid_layout2 = nullptr;
    QVBoxLayout* m_grid_layout3 = nullptr;
    QTabWidget* m_tab1 = nullptr;
    QTabWidget* m_tab2 = nullptr;
    QWidget* m_w1 = nullptr;
    QWidget* m_w2 = nullptr;
    QWidget* m_w3 = nullptr;
    QWidget* m_w4 = nullptr;
    QComboBox* m_dropdown_memu = nullptr;
    QTextEdit* m_text_edit = nullptr;

    ros::Publisher m_pub_ts;
    geometry_msgs::TwistStamped m_msg_ts;
    ros::NodeHandle m_nh;
    ros::ServiceClient m_srv;

    QGroupBox* m_group1;
    QGroupBox* m_group2;
    QGroupBox* m_group3;

    std::vector<double>* m_vmax;
    std::vector<double>* m_pmax;
    std::vector<double>* m_pmin;
    urdf::ModelInterfaceSharedPtr m_robot_model;
    std::string m_path_to_config;
    std::vector<std::string>* m_controlled_joints;
    int m_nAx;
    bool m_check1;

    std::vector<std::string> m_jc_hi; // hi with a teleop joint or cartesian controller
    std::vector<std::string> m_jc_hi_ctrl;
    std::vector<std::string> m_jc2_hi_ctrl;
    std::vector<std::string> m_jc2_hi_ctrl_types;
    std::vector<std::string> m_jc2_hi;
    std::vector<std::string> m_jc_ctrl;
    std::vector<std::string> m_jc2_ctrl;
    std::vector<std::string> m_jc2_conf;

    std::vector<std::string> m_bool1;
    std::vector<std::string> m_bool1_hi;
    std::vector<std::string> m_bool1_conf;
    std::vector<std::string> m_bool2_conf;
    std::vector<std::string> m_bool2;

    std::vector<joint_limits_interface::JointLimits> m_joints_limits;

    QPushButton* m_button;

    ros::ServiceClient m_srv_client_start;
    configuration_msgs::StartConfiguration m_srv_start;







  };
}
  
#endif
