#ifndef jo_teleop_gui_201811080926
#define jo_teleop_gui_201811080926

#include <ros/ros.h>
#include <ros/package.h>
#include <sensor_msgs/JointState.h>
#include <joint_limits_interface/joint_limits.h>
#include <subscription_notifier/subscription_notifier.h>
#include <QPushButton>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <QTabWidget>
#include <QWidget>
#include <QSlider>
#include <QTextEdit>

#include <basic_panel_gui.h>


namespace joint_teleop_gui
{
class JoTeleop: public QObject
{

  Q_OBJECT

public Q_SLOTS:
  void regulateVelocity();
  void moveJointPrecision();
  void callback(const sensor_msgs::JointStateConstPtr msg);

public:
    JoTeleop (basic_panel_gui::BasicPanel* basic_panel, std::string hi_namespace, std::string ctrl_namespace, std::vector<std::string>* controlled_joints, std::vector<joint_limits_interface::JointLimits> joints_limits);
    virtual ~JoTeleop();

    std::vector<QPushButton*> m_button1;
    std::vector<QPushButton*> m_button2;
    std::vector<QPushButton*> m_button3;
    QLabel* m_label;
    QLabel* m_label1;
    std::vector<QLabel*> m_label2;
    std::vector<QSlider*> m_slider_limits;
    QLabel* m_label3;
    QGridLayout* m_grid_layout1;
    QGridLayout* m_grid_layout2;
    QGridLayout* m_grid_layout3;
    QGridLayout* m_grid_layout4;
    QGridLayout* m_grid_layout5;
    QGroupBox* m_group1;
    QGroupBox* m_group2;
    std::vector<QTextEdit*> m_txt_edit1;
    bool m_repeat = false;
    QTabWidget* m_tab;
    QWidget* m_w1;
    QWidget* m_w2;
    QIcon* m_button_icon;
    QPixmap* m_pixmap;
    QSlider* m_slider;
    QTextEdit* m_txt_edit;

    std::vector<std::string>* m_controlled_joints;
    int m_nAx;
    std::vector<double>* m_vmax;
    std::vector<double>* m_pmax;
    std::vector<double>* m_pmin;
    std::vector<double> m_fb_pos;

    ros::NodeHandle m_nh;
    ros::Publisher m_pub_js;
    sensor_msgs::JointState m_msg_js;
    std::string m_hi_namespace;
    std::string m_ctrl_namespace;

    std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_joint_target_rec;


};
}

#endif
