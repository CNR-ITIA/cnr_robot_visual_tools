#ifndef te_teleop_gui_201901111133
#define te_teleop_gui_201901111133

#include <ros/ros.h>
#include <ros/package.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/JointState.h>
#include <dirent.h>
#include <joint_limits_interface/joint_limits.h>
#include <QPushButton>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QHeaderView>
#include <QSlider>
#include <QTextEdit>

#include <basic_panel_gui.h>


namespace teach_teleop_gui
{
class TeTeleop: public QObject
{

  Q_OBJECT

public Q_SLOTS:
  void savePosition();
  void deletePosition();
  void moveToPosition();
  void loadPositions();
  void registerPosition();
//  void loadParameter(std::string load_file_name, std::string load_parameter_name);


public:

    TeTeleop (basic_panel_gui::BasicPanel *basic_panel, std::string hi_namespace, std::string ctrl_namespace, std::vector<std::string> *controlled_joints, std::vector<joint_limits_interface::JointLimits> joints_limits);
    virtual ~TeTeleop();
    void callback(const sensor_msgs::JointStateConstPtr msg);

    QPushButton* m_button1;
    QPushButton* m_button2;
    QPushButton* m_button3;
    QPushButton* m_button4;
    QPushButton* m_button99;
    QGridLayout* m_grid_layout1;
    QGridLayout* m_grid_layout2;
    QVBoxLayout* m_grid_layout3;
    QGridLayout* m_grid_layout4;
    QGroupBox* m_group1;
    QGroupBox* m_group2;
    QTextEdit* m_txt_edit1;
    QTableWidget* m_table;
    QTableWidgetItem* m_table_item;
    QTableWidgetItem* m_table_item2;
    QIcon* m_button_icon;

    bool m_repeat = false;
    int m_row_count = 0;

    QSlider* m_slider;
    QTextEdit* m_txt_edit;

    std::vector<std::string>* m_controlled_joints;
    std::vector<double> m_target_pos;
    int m_nAx;
    std::vector<double>* m_vmax;
    double m_vmax_minimum;

    ros::NodeHandle m_nh;
    ros::Publisher m_pub_js;
    sensor_msgs::JointState m_msg_js;

    std::shared_ptr<ros_helper::SubscriptionNotifier<sensor_msgs::JointState>> m_joint_target_rec;
    std::string m_path_to_config;
    std::string m_hi_namespace;
    std::string m_ctrl_namespace;

    bool m_load = false;

};
}

#endif
