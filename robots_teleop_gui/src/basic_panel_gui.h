#ifndef basic_panel_gui_201811080926
#define basic_panel_gui_201811080926

#include <QString>
#include <QSlider>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QBoxLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>


namespace basic_panel_gui
{
class BasicPanel : public QObject
{

Q_OBJECT

public Q_SLOTS:
  void overrideVelocity();


public:
  BasicPanel();
  virtual ~BasicPanel();

  QSlider* m_slider;
  QLabel* m_label;
  QVBoxLayout* m_grid_layout1;
  QGridLayout* m_grid_layout2;
  QGridLayout* m_grid_layout3;
  QGridLayout* m_grid_layout4;
  QHBoxLayout* m_layout5;
  QVBoxLayout* m_layout6;
  QVBoxLayout* m_layout7;
  QVBoxLayout* m_layout8;
  QGroupBox* m_group1;
  QGroupBox* m_group2;
  QGroupBox* m_group3;
  QGroupBox* m_group4;
  QGroupBox* m_group5;
  QTextEdit* m_txt_edit;

};
}

#endif

