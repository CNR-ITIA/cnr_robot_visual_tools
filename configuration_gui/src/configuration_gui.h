#ifndef configuration_gui_201811080926
#define configuration_gui_201811080926

#include <ros/ros.h>
#include <std_srvs/SetBool.h>
#include <configuration_msgs/ListConfigurations.h>
#include <configuration_msgs/StartConfiguration.h>
#include <configuration_msgs/StopConfiguration.h>

#include "rviz/panel.h"
#include "rviz/visualization_manager.h"

#include <QVBoxLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>
#include <QTextEdit>
#include <QTimer>

// class QPushButton;
namespace configuration_gui
{
  
  class ConfigurationManager: public rviz::Panel
  {
    
  Q_OBJECT
  public:
    
    ConfigurationManager( QWidget* parent = 0 );
    virtual ~ConfigurationManager();
    virtual void onInitialize();    
    /** @brief Load the given html file. */

    
  protected Q_SLOTS:
    void startConfiguration();
    void updateConfigurations();
    void stopConfiguration();
    void callback();
    void updateSelectedCallback();
    
  protected:  
    ros::NodeHandle m_nh;
    
    QPushButton* m_button;
    QVBoxLayout* m_vbox;
    QGroupBox* m_groupBox;
    QVBoxLayout* m_layout;
    QTextEdit* m_txt_edit;
    
    ros::ServiceClient m_list_config;
    ros::ServiceClient m_start_config;
    ros::ServiceClient m_stop_config;

    
    configuration_msgs::ListConfigurations m_list;
    configuration_msgs::StartConfiguration m_start;
    configuration_msgs::StopConfiguration m_stop;
    
    bool m_cfg_load;
    
    std::vector<QRadioButton*> m_configs;
    std::vector<size_t>        m_button_to_conf;

    QTimer *m_timer;
  };
  
}

#endif
