#include <configuration_gui.h>

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(configuration_gui::ConfigurationManager,rviz::Panel )


namespace configuration_gui
{

ConfigurationManager::ConfigurationManager(QWidget* parent):
Panel(parent),
m_button( NULL )
{
  m_layout = new QVBoxLayout( this );
  
  // botton
  m_button = new QPushButton();
  m_button->setText("Start Configuration");
  connect(m_button, SIGNAL(clicked()), this, SLOT(startConfiguration()));
  m_layout->addWidget( m_button );
  
  m_button = new QPushButton();
  m_button->setText("Update Configurations");
  connect(m_button, SIGNAL(clicked()), this, SLOT(updateConfigurations()));
  m_layout->addWidget( m_button );

  m_button = new QPushButton();
  m_button->setText("Stop Configuration");
  connect(m_button, SIGNAL(clicked()), this, SLOT(stopConfiguration()));
  m_layout->addWidget( m_button );
  
  m_txt_edit = new QTextEdit();
  m_txt_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Maximum);
  m_layout->addWidget(m_txt_edit);
  
  // radio button
  m_groupBox = new QGroupBox(tr("List of available configurations"));
  
  m_list_config  = m_nh.serviceClient<configuration_msgs::ListConfigurations>("/configuration_manager/list_configurations");
  m_list_config.call(m_list);
  
  m_start_config = m_nh.serviceClient<configuration_msgs::StartConfiguration>("/configuration_manager/start_configuration");

  m_stop_config = m_nh.serviceClient<configuration_msgs::StopConfiguration>("/configuration_manager/stop_configuration");
 
  m_vbox = new QVBoxLayout;
  m_cfg_load=false;
  updateConfigurations();
  
  m_timer = new QTimer(this);
  connect(m_timer, SIGNAL(timeout()), this, SLOT(updateSelectedCallback()));
  m_timer->start(1000);
}


ConfigurationManager::~ConfigurationManager()
{
  
}

void ConfigurationManager::onInitialize()
{
  
}


void ConfigurationManager::startConfiguration()
{
  if (!m_cfg_load)
  {
    m_txt_edit->setText("No configurations to start");
    ROS_WARN("No configurations to start");
  }
  else
  {
    //for (unsigned int icfg=0; icfg < m_configs m_list.response.configurations.size(); icfg++)
    for( unsigned int ibtn=0; ibtn < m_configs.size(); ibtn++ )
    {
      size_t icfg = m_button_to_conf.at( ibtn );
      if (m_configs.at( ibtn )->isChecked())
      {
        m_start.request.start_configuration = m_list.response.configurations.at(icfg).name;
        m_start.request.strictness          = 1;
        m_start_config.call( m_start );
        m_txt_edit->setText("Configuration <html><b>"+QString::fromStdString(m_list.response.configurations.at(icfg).name)+"</b></html> correctly started ");
        ROS_INFO("Configuration %s correctly started", m_list.response.configurations.at(icfg).name.c_str());
        m_timer->start(2000);
      }   
    }
  }
}

void ConfigurationManager::stopConfiguration()
{
  if (!m_cfg_load)
  {
    m_txt_edit->setText("No configurations to stop");
    ROS_WARN("No configurations to stop");
  }
  else
  {
    m_stop.request.strictness=1;
    m_stop_config.call( m_stop );
    m_txt_edit->setText("Configuration correctly stopped");
    ROS_INFO("Configuration correctly stopped");
  }
}


void ConfigurationManager::updateSelectedCallback()
{

  if (m_list_config.call(m_list))
  {
  for (unsigned int idx=0;idx<m_list.response.configurations.size();idx++)
  {
    const configuration_msgs::ConfigurationComponent& cfg=m_list.response.configurations.at(idx);
    if (!cfg.state.compare("running"))
    {
      m_txt_edit->setText("Active Configuration is <html><b>"+QString(cfg.name.c_str())+"</b></html>");
    }
  }
  }
  else
  {
    m_txt_edit->setText("Unable to contact configuration manager");
  }
  
}


  void ConfigurationManager::updateConfigurations()
  {
    m_list_config.call(m_list);
    if (m_list.response.configurations.size() > 0)
    {
      if (!m_cfg_load)
      {
        callback();
        m_txt_edit->setText("Configurations correctly loaded. Chose a desired configuration.");
        ROS_INFO("Configurations correctly loaded");
      }
      else
      {
        m_txt_edit->setText("Configurations already loaded");
        ROS_WARN("Configurations already loaded");
      }
    }
    else
    {
      m_txt_edit->setText("No configurations loaded. Load a feasibile configuration then: <html><b>Update Configurations</b></html>.");
      ROS_WARN("No configurations loaded");
    }  
    
  }
  
  
  void ConfigurationManager::callback()
  {

    m_list_config.call(m_list);

    m_button_to_conf.clear( );
    m_configs.clear( );
    
    for (unsigned int icfg=0; icfg<m_list.response.configurations.size(); icfg++)
    {
      std::cout << "Ctrl " << m_list.response.configurations.at(icfg).name << "is hidden? " <<
                   (m_list.response.configurations.at(icfg).hidden ? "TRUE" : "FALSE" ) << std::endl;
      if( m_list.response.configurations.at(icfg).hidden )
      {
        continue;
      }
      m_configs.push_back( new QRadioButton(tr(m_list.response.configurations.at(icfg).name.c_str()) ) );
      m_vbox->addWidget(m_configs.back( ) );
      m_button_to_conf.push_back( icfg );
    }
    m_vbox->addStretch(1);
    m_groupBox->setLayout(m_vbox);
    
    m_layout->addWidget( m_groupBox );
    m_configs.front()->setChecked(true); //default??
    
    m_cfg_load=true;
  }


}



